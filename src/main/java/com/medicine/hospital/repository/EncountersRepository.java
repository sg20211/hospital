package com.medicine.hospital.repository;

import com.medicine.hospital.model.Encounters;
import org.springframework.data.repository.CrudRepository;


public interface EncountersRepository extends CrudRepository<Encounters,Long> {

}
