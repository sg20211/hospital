package com.medicine.hospital.repository;

import com.medicine.hospital.model.Providers;
import org.springframework.data.repository.CrudRepository;

public interface ProvidersRepository extends CrudRepository<Providers,Long> {
}
